<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests\CheckArticlesRequest;
use App\Articles;
use Carbon\Carbon;

class ArticlesController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index() {
		//
		$articles = Articles::all();
		return view('pages.articles')->with('articles',$articles);
	}

	public function create(){
		return view('pages.create');
	}

	public function store(CheckArticlesRequest $request){

		//lay du lieu vao
		$inputData = $request->all();
		unset($inputData['_token']);

		//Nhap du lieu vao bang articles = C1:
		/*$articles = new Articles();
		$articles->name = $inputData['name'];
		$articles->author = $inputData['author'];
		$articles->save();
		*/
		// C2:
		Articles::create($inputData);

		return redirect('articles');

		/*echo '<pre>';
		print_r($inputData);
		echo '</pre>';*/
		
	}


	public function edit($id){
		$articles = Articles::findOrfail($id);
		return view('pages.edit',compact('articles'));
	}

	public function update($id, Request $request){

		//lay du lieu vao
		$inputData = $request->all();
		unset($inputData['_token']);
		unset($inputData['_method']);
		unset($inputData['updated_at']);
		unset($inputData['created_at']);

		echo '<pre>';
		print_r($inputData);
		echo '</pre>';
		
		$articles = Articles::findOrfail($id);

		echo '<pre>';
		print_r($articles->toArray());
		echo '</pre>';
		//exit;
		$articles->update($inputData);
		return redirect('articles');
 
	}

}
