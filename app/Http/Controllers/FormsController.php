<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests\CheckArticlesRequest;
use App\Articles;
use Carbon\Carbon;

class FormsController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index() {
		//
		$articles = Articles::all();
		return view('pages.articles')->with('articles',$articles);
	}

	public function form1(){
		return view('form.001');
	}

	

}
