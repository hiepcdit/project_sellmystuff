<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class PagesController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	
	public function index()
	{
		return view('home');
	}
	public function aboutme(){
		$data = 'my nam is Hiep!';
		$age = 26;
		//dua du lieu ra view

		//C1: return view('pages.aboutme')->with(['name'=>$data,'age'=>$age]);
		//C2: return view('pages.aboutme',['name'=>$data,'age'=>$age]);
	}

	public function about(){
		return view('pages.about');
	}

	public function contact(){
		return view('pages.contact');
	}


}
