<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Articles extends Model {

	//
	protected $fillable = ['name','author','created_at','undated_at'];

	public function setCreatedAtAttribute($date){
		$this->attributes['created_at'] = Carbon::createFromFormat('Y-m-d',$date);
	}

	// public function setUpdatedAtAttribute($date){
	// 	$this->attributes['updated_at'] = Carbon::createFromFormat('Y-m-d',$date);
	// }
}
