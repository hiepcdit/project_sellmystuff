@extends('template.master')
@section('content')
<section>
	<h1>Them bai viet moi!</h1>
	<div id='form'>
	{!! Form::open(['url'=>'articles']) !!}
		{!! Form::label('name','Name:') !!}<br/>
		{!! Form::text('name') !!}<br/>
		{!! Form::label('author','Author: ') !!}<br/>
		{!! Form::input('text','author') !!}<br/>
		{!! Form::label('created_at','Created_at:') !!}<br/>
		{!! Form::input('date','created_at',date('Y-m-d')) !!}<br/>
		<br/><br/>
		{!! Form::submit('Them moi')!!}
	{!! Form::close() !!}
	</div>
	@if($errors->any())
		<ul>
			@foreach ($errors->all() as $error)
			<li style="color: red">{{ $error }}</li>
			@endforeach
		</ul>
	@endif
</section>
@stop