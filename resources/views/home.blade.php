<!DOCTYPE html>
<html lang="">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Title Page</title>

	<!-- Bootstrap CSS -->
	<link href="/css/bootstrap.min.css" rel="stylesheet">
	<link href="/css/app.css" rel="stylesheet">

	<!-- jQuery -->
	<script src="js/jquery.js"></script>
	<!-- Bootstrap JavaScript -->
	<script src="js/bootstrap.min.js"></script>
	<script src="js/back-top.js"></script>

</head>
<body>
	<!-- ====================== Navbar =============== -->
	<nav class="navbar navbar-default" role="navigation">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="#"><span class="glyphicon glyphicon-home" aria-hidden="true"></span></a>
		</div>
		
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse navbar-ex1-collapse">
			<ul class="nav navbar-nav">
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">Thời trang</a>
					<ul class="dropdown-menu">
						<li><a href="#">Viet Nam</a></li>
						<li><a href="#">English</a></li>
						<li><a href="#">Japanese</a></li>
						<li><a href="#">Chinese</a></li>
					</ul>

				</li>
				<li><a href="#">Công Nghệ</a></li>
				<li><a href="#">Dịch vụ</a></li>
				<li><a href="#">Mua sắm</a></li>
				<li><a href="#">Phố đồ cũ</a></li>
				<li><a href="#">Liên hệ</a></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li class="up"><a href="#">Đăng tin</a></li>
				<li><a href="#">Đăng Nhập</a></li>
				<li><a href="#">Đăng Ký</a></li>

				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">Language <b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li><a href="#">Viet Nam</a></li>
						<li><a href="#">English</a></li>
						<li><a href="#">Japanese</a></li>
						<li><a href="#">Chinese</a></li>
					</ul>
				</li>
			</ul>
		</div><!-- /.navbar-collapse -->
	</nav>
	<!-- ====================== form search =============== -->
	<div class="container">
		<div class="row">
			<div class="panel panel-default">
				<div class="panel-body">
					<span class="glyphicon glyphicon-home" style="font-size: 25px;color:blue;top:5px;padding:0px;margin:0 10px" aria-hidden="true"></span>
					<a href="#">Trang chủ</a> >>
					<a href="#">Phố đồ cũ</a> >>
					<a href="#">Tìm kiếm</a>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-9 col-xs-9 row-search">
				<form class="navbar-form navbar-left" role="search">
					<input type="text" class="form-control sr-text" placeholder="Hôm nay bạn muốn mua gì?">
					<div class="form-group search">
						<input type="text" class="form-control" placeholder="Tất cả các địa điểm">
						<input type="text" class="form-control" placeholder="Ngày đăng">
						<input type="text" class="form-control" placeholder="Tất cả danh mục">
						<input type="text" class="form-control" placeholder="Sản phẩm">

					</div>

					<div class="btn-search">
						<button type="submit" class="btn btn-danger">Tìm kiếm</button>
						<a href="#" class="btn btn-warning" style="margin-left:20px; width: 25% !important;">Đăng tin</a>
					</div>
				</form>
			</div>
			<!-- ====================== form Login =============== -->
			<div class="col-md-3 login">

				<form action="#" method="POST" role="form">
					<legend>Đăng nhập tài khoản</legend>
					<div class="form-group">
						<label for="">Username:</label>
						<input type="text" class="form-control username" name ="username" id="username" placeholder="Tài khoản">
						<label for="">Password:</label>
						<input type="password" class="form-control password" name ="password" id="password" placeholder="Mật khẩu">
					</div>
					<button type="submit" class="btn btn-primary">Đăng nhập</button>
					<a href="">Đăng ký</a>
				</form>
			</div>
		</div>

		<!-- ===============Phan noi dung =================== -->
		<div class="row">
			<hr/>

			<div class="col-md-9 content">
				<!-- =============Noi dung post/search ============ -->
				<div class="row title-content">
					<a href=""><span class="panel-primary" >Máy tính, thiết bị văn phòng</span></a>
				</div>
				<div class="thumbnail">

					<img data-src="#" alt="">
					<div class="caption post">
						<img src="images/01.jpg" />
						<a href=""><h5>Bán case đồng bộ core i5 và UPS giá 200.000vnd P V gia đình</h5></a>
						<span>Địa điểm: Đà Nẵng, Hồ Chí Minh, Hà Nội.</span>
						<div class="col-md-5 info">
							<ul>
								<li class="date glyphicon glyphicon-calendar"> Ngày đăng: <i>22/12/2015</i></li>
								<li class="see glyphicon glyphicon-eye-open"> Số lượng xem: <i>200</i></li>
							</ul>
						</div>

						<div class="col-md-5 info2">
							<ul>
								<li class="user glyphicon glyphicon-user"> Đăng bởi: <i>Duchiep</i></li>
								<li class="like glyphicon glyphicon-thumbs-up"> Lượt thích: <i>250</i></li>
							</ul>
						</div>
					</div>
				</div><!-- end thumbnail -->
				<!-- =============End Noi dung post/search ============ -->

				<!-- =============Noi dung post/search ============ -->

				<div class="thumbnail">

					<img data-src="#" alt="">
					<div class="caption post">
						<img src="images/01.jpg" />
						<a href=""><h5>Bán case đồng bộ core i5 và UPS giá 200.000vnd P V gia đình</h5></a>
						<span>Địa điểm: Đà Nẵng, Hồ Chí Minh, Hà Nội.</span>
						<div class="col-md-5 info">
							<ul>
								<li class="date glyphicon glyphicon-calendar"> Ngày đăng: <i>22/12/2015</i></li>
								<li class="see glyphicon glyphicon-eye-open"> Số lượng xem: <i>200</i></li>
							</ul>
						</div>

						<div class="col-md-5 info2">
							<ul>
								<li class="user glyphicon glyphicon-user"> Đăng bởi: <i>Duchiep</i></li>
								<li class="like glyphicon glyphicon-thumbs-up"> Lượt thích: <i>250</i></li>
							</ul>
						</div>
					</div>
				</div><!-- end thumbnail -->
				<!-- =============End Noi dung post/search ============ -->

				<!-- =============Noi dung post/search ============ -->
				<div class="thumbnail">

					<img data-src="#" alt="">
					<div class="caption post">
						<img src="images/01.jpg" />
						<a href=""><h5>Bán case đồng bộ core i5 và UPS giá 200.000vnd P V gia đình</h5></a>
						<span>Địa điểm: Đà Nẵng, Hồ Chí Minh, Hà Nội.</span>
						<div class="col-md-5 info">
							<ul>
								<li class="date glyphicon glyphicon-calendar"> Ngày đăng: <i>22/12/2015</i></li>
								<li class="see glyphicon glyphicon-eye-open"> Số lượng xem: <i>200</i></li>
							</ul>
						</div>

						<div class="col-md-5 info2">
							<ul>
								<li class="user glyphicon glyphicon-user"> Đăng bởi: <i>Duchiep</i></li>
								<li class="like glyphicon glyphicon-thumbs-up"> Lượt thích: <i>250</i></li>
							</ul>
						</div>
					</div>
				</div><!-- end thumbnail -->
				<!-- =============End Noi dung post/search ============ -->

				<!-- =============Noi dung post/search ============ -->
				<div class="row title-content">
					<a href=""><span>Điện thoại - Máy tính</span></a>
				</div>
				<div class="thumbnail">

					<img data-src="#" alt="">
					<div class="caption post">
						<img src="images/01.jpg" />
						<a href=""><h5>Bán case đồng bộ core i5 và UPS giá 200.000vnd P V gia đình</h5></a>
						<span>Địa điểm: Đà Nẵng, Hồ Chí Minh, Hà Nội.</span>
						<div class="col-md-5 info">
							<ul>
								<li class="date glyphicon glyphicon-calendar"> Ngày đăng: <i>22/12/2015</i></li>
								<li class="see glyphicon glyphicon-eye-open"> Số lượng xem: <i>200</i></li>
							</ul>
						</div>

						<div class="col-md-5 info2">
							<ul>
								<li class="user glyphicon glyphicon-user"> Đăng bởi: <i>Duchiep</i></li>
								<li class="like glyphicon glyphicon-thumbs-up"> Lượt thích: <i>250</i></li>
							</ul>
						</div>
					</div>
				</div><!-- end thumbnail -->

				<div class="thumbnail">

					<img data-src="#" alt="">
					<div class="caption post">
						<img src="images/01.jpg" />
						<a href=""><h5>Bán case đồng bộ core i5 và UPS giá 200.000vnd P V gia đình</h5></a>
						<span>Địa điểm: Đà Nẵng, Hồ Chí Minh, Hà Nội.</span>
						<div class="col-md-5 info">
							<ul>
								<li class="date glyphicon glyphicon-calendar"> Ngày đăng: <i>22/12/2015</i></li>
								<li class="see glyphicon glyphicon-eye-open"> Số lượng xem: <i>200</i></li>
							</ul>
						</div>

						<div class="col-md-5 info2">
							<ul>
								<li class="user glyphicon glyphicon-user"> Đăng bởi: <i>Duchiep</i></li>
								<li class="like glyphicon glyphicon-thumbs-up"> Lượt thích: <i>250</i></li>
							</ul>
						</div>
					</div>
				</div><!-- end thumbnail -->

				<!-- =============End Noi dung post/search ============ -->

			</div><!-- end .content -->



			<!-- ====================== saibar =============== -->
			<div class="col-md-3 saibar">
				<div class="facebook">
					<div id="fb-root"></div>
					<script>
						(function(d, s, id) {
							var js, fjs = d.getElementsByTagName(s)[0];
							if (d.getElementById(id)) return;
							js = d.createElement(s); js.id = id;
							js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.3";
							fjs.parentNode.insertBefore(js, fjs);
						}(document, 'script', 'facebook-jssdk'));
					</script>
					<div class="fb-follow" data-href="https://www.facebook.com/zuck" data-width="200px" data-colorscheme="light" data-layout="standard" data-show-faces="true">

					</div>
				</div>
				<br>
				<div class="list-group">
					<a href="#" class="list-group-item active">Tin xem nhiều nhất</a>
					<a href="#" class="list-group-item">Item 2</a>
					<a href="#" class="list-group-item">Item 3</a>
					<a href="#" class="list-group-item">Item 4</a>
					<a href="#" class="list-group-item">Item 5</a>
					<a href="#" class="list-group-item">Item 6</a>
					<a href="#" class="list-group-item">Item 7</a>
				</div>

			</div>
		</div>
	</div>

	<!-- ====================== Footer =============== -->
	<div class="panel-footer footer">

		<div class="row link-footer">
			<div class="container">
				<ul class="link-left-footer">
					<li>
						<a href="">Trang chủ</a>
						<a href="">Phố đổ củ</a>
						<a href="">Tìm kiếm sản phẩm</a>
					</li>
				</ul>

				<ul class="link-right-footer">
					<li>
						<a href="">Quy định</a>
						<a href="">Quy chế</a>
						<a href="">Hướng dẫn sử dụng</a>
						<a href="">Liên hệ quảng cáo & dịch vụ</a>
					</li>
				</ul>
			</div>
		</div>

		
		<div class="container">
			<div class="row">
			<div class="footerMenu">
				
				<ul class="items level-0">
					<li class="level-1 col-md-3"><a href="">Thời trang nam</a>
						<ul class="items level-2">
							<li class="level-2 node_17"><a href="">Quần áo</a></li>
							<li class="level-2 node_15"><a href="">Giầy dép</a></li>
							<li class="level-2 node_14"><a href="">Đồng hồ và Trang sức</a></li>
							<li class="level-2 node_122"><a href="">Đồ thể thao</a></li>
							<li class="level-2 node_16"><a href="">Phụ kiện thời trang</a></li>
						</ul>
					</li>
					<li class="level-1 col-md-3"><a href="">Thời trang nữ</a>
						<ul class="items level-2">
							<li class="level-2 node_50"><a href="">Quần, Áo, Váy</a></li>
							<li class="level-2 node_52"><a href="">Giầy dép</a></li>
							<li class="level-2 node_124"><a href="">Đồ thể thao</a></li>
							<li class="level-2 node_49"><a href="">Trang sức và Phụ kiện</a></li>
							<li class="level-2 node_51"><a href="">Nước hoa, Mỹ phẩm</a></li>
						</ul>
					</li>
					<li class="level-1 col-md-3"><a href="">Thời trang cho bé</a>
						<ul class="items level-2">
							<li class="level-2 node_126"><a href="">Cho bé sơ sinh</a></li>
							<li class="level-2 node_127"><a href="">Cho bé gái</a></li>
							<li class="level-2 node_128"><a href="">Cho bé trai</a></li>
						</ul>
					</li>
					<li class="level-1 col-md-3"><a href="">Điện thoại - Máy tính bảng</a>
						<ul class="items level-2">
							<li class="level-2 node_56"><a href="">SmartPhone, Cao cấp</a></li>
							<li class="level-2 node_94"><a href="">Điện thoại bình dân</a></li>
							<li class="level-2 node_58"><a href="">Máy tính bảng, Máy đọc sách</a></li>
							<li class="level-2 node_57"><a href="">Linh kiện, Phụ kiện</a></li>
						</ul>
					</li>
				</ul>

			</div>
			</div>
		</div> <!-- End Container -->

	</div><!-- End .footer -->
	<div id="backtop">
		<img src="images/back-top.png" alt="">
	</div>
</body>
</html>