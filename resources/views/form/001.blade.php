<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Form</title>
	<link rel="stylesheet" href="">
</head>
<body>
	{!! Form::open(array('url' => 'form2')) !!}
		
	{!! Form::close() !!}

	{!! Form::open(array('route' => 'form1')) !!}
		
	{!! Form::close() !!}

	{!! Form::open(array('action' => 'FormsController@form1')) !!}
		
	{!! Form::close() !!}

	{!! Form::open(array('action' => 'ArticlesController@index','method' => 'GET')) !!}
		
	{!! Form::close() !!}
	
	{!! Form::open(array('action' => 'ArticlesController@index','files'=>true)) !!}
		
	{!! Form::close() !!}

	{!! Form::open(array('route' => 'form1','method'=>'PUT')) !!}
		
	{!! Form::close() !!}

	{{ Form::model($Forms, array('route' => array('form1', 4))) }}
	{{ Form::close() }}
</body>
</html>